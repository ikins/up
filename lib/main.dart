import 'package:flutter/material.dart';
import 'package:up/routes/routes.dart';
//
import 'package:up/views/main/up.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'UP',
        theme: ThemeData(primaryColor: Colors.white),
        initialRoute: Routes.up,
        routes: {Routes.up: (context) => UpPage()});
  }
}
