import 'package:http/http.dart' as http;
import 'package:up/network/api.dart';

class Network {
  final String _url = Api.urlDefault;

  getData(apiUrl) async {
    var fullUrl = _url + apiUrl;
    return await http.get(fullUrl, headers: _setHeaders());
  }

  _setHeaders() => {
        'content-type': 'application/json',
        'accept': 'application/json',
      };
}
